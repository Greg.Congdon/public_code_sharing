"""
insertCohesive script is used to read an LS-DYNA keyword file and insert
cohesive elements in between existing shell elements.

Author:     Greg Congdon
Date:       2021-03-22

"""

import numpy as np
from math import hypot, atan2, pi, floor

def nodeShift(node: dict, v: np.ndarray) -> np.ndarray:
    """
    This function takes a node dictionary object and a translation vector
    and returns a vector of translated coordinates.
    """

    x = node['x']
    y = node['y']
    z = node['z']

    v_out = np.zeros(3)
    v_out[0] = x + v[0]
    v_out[1] = y + v[1]
    v_out[2] = z + v[2]

    return v_out

class dynaModel:
    def __init__(self):
        self.NODES = {}
        self.SHELLS = {}
        self.BEAMS = {}
        self.SET_NODES = {}
        self.NRBS = {}
        self.shell_buckets = {}
        self.theta_buckets = {}
        self.theta_buckets_reduced = {}
        self.NID_max = 0
        self.sid_max = 0
        self.sid_old_max = 0
        self.bid_max = 0
        self.set_max = 0
        self.nrb_max = 0

        self.wall_shell_thickness = 0.0
        self.theta_offest = 0.0

        self.currentKeyword = None
        self.west_beam_write = True

        self.skipped_rows = 0
        
        self.skipped_rows_print_suppression = {}
        self.bs_counter = 0

        self.debugging_bed = False
        self.debugging_head = False

    def parseLine(self, line: str) -> None:
        if line[0] == '$':
            return
        elif line[0] == '*':
            self.currentKeyword = line.replace('*', '').replace('\n', '')
            return

        if 'NODE' in self.currentKeyword:
            self.buildNode(line)
        elif 'ELEMENT_SHELL' in self.currentKeyword:
            self.buildShell(line)
        else:
            return
    
    def buildNode(self, line: str) -> None:
        nodeFWF = [0,8,24,40,56]
        chunks = [line[nodeFWF[k]:nodeFWF[k+1]].replace(' ', '') for k in range(0, (len(nodeFWF)-1))]

        nid = chunks[0]
        self.NODES[nid] = {'nid': int(nid)}
        self.NODES[nid]['x'] = float(chunks[1])
        self.NODES[nid]['y'] = float(chunks[2])
        self.NODES[nid]['z'] = float(chunks[3])
        self.NODES[nid]['XREF'] = []
        self.NODES[nid]['HEAD_XREF'] = []
        self.NODES[nid]['NEW'] = False

        if self.NODES[nid]['nid'] > self.NID_max:
            self.NID_max = self.NODES[nid]['nid']

    def buildShell(self, line: str) -> None:
        shellFWF = [0, 8, 16, 24, 32, 40, 48]
        chunks = [line[shellFWF[k]:shellFWF[k+1]].replace(' ', '') for k in range(0, (len(shellFWF)-1))]

        sid = chunks[0]

        self.SHELLS[sid] = {'sid': int(sid)}
        self.SHELLS[sid]['pid'] = int(chunks[1])
        self.SHELLS[sid]['n1'] = chunks[2]
        self.SHELLS[sid]['n2'] = chunks[3]
        self.SHELLS[sid]['n3'] = chunks[4]
        self.SHELLS[sid]['n4'] = chunks[5]
        self.SHELLS[sid]['TOPOLOGY'] = [chunks[2],chunks[3],chunks[4],chunks[5]]
        self.SHELLS[sid]['BED_ref'] = ''
        self.SHELLS[sid]['HEAD_ref'] = ''

        # Create cross-references on node elements for easy access
        self.NODES[self.SHELLS[sid]['n1']]['XREF'].append(sid)
        self.NODES[self.SHELLS[sid]['n2']]['XREF'].append(sid)
        self.NODES[self.SHELLS[sid]['n3']]['XREF'].append(sid)
        self.NODES[self.SHELLS[sid]['n4']]['XREF'].append(sid)

        if self.SHELLS[sid]['sid'] > self.sid_max:
            self.sid_max = self.SHELLS[sid]['sid']
            self.sid_old_max = self.sid_max

        self.shellCentroid(sid)

    # Create new node from existing coincident node
    def buildCoincidentNode(self, old_nid: str) -> None:
        self.NID_max += 1
        new_nid = str(self.NID_max)

        self.NODES[new_nid] = {'nid': self.NID_max}
        self.NODES[new_nid]['x'] = self.NODES[old_nid]['x']
        self.NODES[new_nid]['y'] = self.NODES[old_nid]['y']
        self.NODES[new_nid]['z'] = self.NODES[old_nid]['z']
        self.NODES[new_nid]['XREF'] = []
        self.NODES[new_nid]['NEW'] = True
        self.NODES[new_nid]['HEAD_XREF'] = []

    def buildNewNode(self, nidInt: int, x: float, y: float, z: float) -> None:        
        """
        This function builds a new unique node.
        """

        nid = str(nidInt)
        self.NODES[nid] = {'nid': nidInt}
        self.NODES[nid]['x'] = x
        self.NODES[nid]['y'] = y
        self.NODES[nid]['z'] = z
        self.NODES[nid]['XREF'] = []
        self.NODES[nid]['HEAD_XREF'] = []
        self.NODES[nid]['NEW'] = True

    def shellCentroid(self, sid: str) -> None:
        n1 = self.NODES[self.SHELLS[sid]['n1']]
        n2 = self.NODES[self.SHELLS[sid]['n2']]
        n3 = self.NODES[self.SHELLS[sid]['n3']]
        n4 = self.NODES[self.SHELLS[sid]['n4']]

        x = [n1['x'],n2['x'],n3['x'],n4['x']]
        y = [n1['y'],n2['y'],n3['y'],n4['y']]
        z = [n1['z'],n2['z'],n3['z'],n4['z']]

        self.SHELLS[sid]['x'] = round(sum(x)/4, 3)
        self.SHELLS[sid]['y'] = round(sum(y)/4, 3)
        self.SHELLS[sid]['z'] = round(sum(z)/4, 3)
        
        if self.SHELLS[sid]['z'] not in self.shell_buckets.keys():
            self.shell_buckets[self.SHELLS[sid]['z']] = [sid]
        else:
            self.shell_buckets[self.SHELLS[sid]['z']].append(sid)

    def modelCentroid(self):
        nodes = list(self.NODES.keys())
        noCnt = len(nodes)

        self.modelX = 0.0
        self.modelY = 0.0
        self.modelZ = 0.0

        for k in nodes:
            self.modelX += self.NODES[k]['x']
            self.modelY += self.NODES[k]['y']
            self.modelZ += self.NODES[k]['z']

        self.modelX /= noCnt
        self.modelY /= noCnt
        self.modelZ /= noCnt

    def cylindSort(self):
        theta_precision = 3

        self.modelCentroid()
        self.brickCourses = list(self.shell_buckets.keys())
        self.brickCourses.sort()
        
        # Optionally winow out rows to be skipped
        if self.skipped_rows > 0:
            new_list = [self.brickCourses[0]]
            totCourses = len(self.brickCourses)
            
            for e in range(1, totCourses):
                if (e%(self.skipped_rows+1)==0):# or (e==(totCourses-1)):
                    new_list.append(self.brickCourses[e])
            
            self.brickCoursesReduced = new_list
            self.brickCoursesReduced.sort()
        
        for k in self.brickCourses:
            self.theta_buckets[k] = {}
            for sid in self.shell_buckets[k]:
                dx = self.SHELLS[sid]['x'] - self.modelX
                dy = self.SHELLS[sid]['y'] - self.modelY
                L = hypot(dx, dy)
                theta_i = atan2(dy, dx) + self.theta_offest

                if theta_i < 0:
                    theta = round(theta_i + 2*pi, theta_precision)
                else:
                    theta = round(theta_i, theta_precision)

                self.theta_buckets[k][theta] = sid 
                
            # Optionally winow out theta buckets to be skipped
            
            if self.skipped_rows > 0:
                allThetas  = list(self.theta_buckets[k].keys())
                allThetas.sort()
                totalTheta = len(allThetas)
                
                new_bucket = {}
                new_bucket[allThetas[0]] = self.theta_buckets[k][allThetas[0]]
                
                for e in range(1, totalTheta):
                    if (e%(self.skipped_rows+1)==0) or (e==(totalTheta-1)):
                        new_bucket[allThetas[e]] = self.theta_buckets[k][allThetas[e]]
                    
                self.theta_buckets_reduced[k] = new_bucket
            
                        

    def cohesivePIDs(self, bedJoint: int, headJoint: int, brickSplit: int) -> None:
        """
        This function takes PID numbers for the new cohesive elements

        brickSplit = PID for vertical cohesive elements representing brick splitting
        bedJoint = PID for horizontal cohesive elements representing mortar joints
        headJoint = PID for vertical cohesive elements representing mortar joints
        """

        self.brickSplitPID = brickSplit
        self.bedJointPID = bedJoint
        self.headJointPID = headJoint
        self.cohesivePIDall = [brickSplit, bedJoint, headJoint]

    def fricBeamPIDs(self) -> None:
        """
        This function assigns PID's to friction beam parts by incrementing
        cohesion PID's.
        """

        self.fricBedPID = round(max(self.cohesivePIDall)+1)
        self.fricHeadPID = round(self.fricBedPID+1)
        self.fricBeamPIDall = [self.fricBedPID, self.fricHeadPID]

    def buildSetNode(self, setID: int, nodeList: list) -> None:
        """
        This function takes a list of nodes and builds a SET_NODE dictionary
        object.
        """

        sid = str(setID)
        self.SET_NODES[sid] = {'sid': setID}
        self.SET_NODES[sid]['nodeList'] = []

        if len(nodeList) <= 8:
            self.SET_NODES[sid]['nodeList'].append(''.join([str(x).rjust(10) for x in nodeList]))
        else:
            eightLim = []
            eightLim.extend([nodeList[(x*8):min((x*8+8),(len(nodeList)))] for x in range(0, floor(len(nodeList)/8)+1)])
            self.SET_NODES[sid]['nodeList'].extend([''.join([str(x).rjust(10) for x in eightLim[e]]) for e in range(0, len(eightLim))])

    def buildNRB(self, nrbIDint: int, setID: int) -> None:
        """
        This function builds a NRB constraint.
        """

        nrbID = str(nrbIDint)
        self.NRBS[nrbID] = {'nrbID': nrbIDint}
        self.NRBS[nrbID]['setID'] = str(setID)

    def buildBeam(self, bidInt: int, pid: int, n1: str, n2: str, n3: str) -> None:
        """
        This function builds a new beam element.
        """

        bid = str(bidInt)
        self.BEAMS[bid] = {'bid': bidInt}
        self.BEAMS[bid]['pid'] = str(pid)
        self.BEAMS[bid]['n1'] = n1
        self.BEAMS[bid]['n2'] = n2
        self.BEAMS[bid]['n3'] = n3

    def buildCohesive(self, pid: int, n1: str, n2: str, n3: str, n4: str) -> None:
        self.sid_max += 1
        sid = str(self.sid_max)
        self.SHELLS[sid] = {'sid': self.sid_max}
        self.SHELLS[sid]['pid'] = pid
        self.SHELLS[sid]['n1'] = n1
        self.SHELLS[sid]['n2'] = n2
        self.SHELLS[sid]['n3'] = n3
        self.SHELLS[sid]['n4'] = n4
        self.SHELLS[sid]['TOPOLOGY'] = [n1,n2,n3,n4]

        self.NODES[n1]['XREF'].append(sid)
        self.NODES[n2]['XREF'].append(sid)
        self.NODES[n3]['XREF'].append(sid)
        self.NODES[n4]['XREF'].append(sid)

    def buildFricBeam(self, nidAll: list, fricPID: int) -> None:
        """
        This function creates new friction beam elements in parallel with 
        cohesive elements.

        nidAll = list of shell node ID's
        [sh1 NW nid, sh1 NE nid, sh1 SE nid, sh1 SW nid, sh2 NW nid, sh2 NE nid]

        New nodes will be created as follows:
            n7 = inside friction beam bottom node (west)
            n8 = inside friction beam top node (west)
            n9 = outside friction beam bottom node (west)
            n10 = outside friction beam top node (west)
            n11 = inside friction beam bottom node (east)
            n12 = inside friction beam top node (east)
            n13 = outside friction beam bottom node (east)
            n14 = outside friction beam top node (east)

        New friction beams will be created as follows:
            b1 = West inside beam
            b2 = West outside beam
            b3 = East inside beam
            b4 = East outside beam
        """

        n1ID = nidAll[0]
        n2ID = nidAll[1]
        n3ID = nidAll[2]
        n4ID = nidAll[3]
        n5ID = nidAll[4]
        n6ID = nidAll[5]

        n1 = self.NODES[n1ID]
        n2 = self.NODES[n2ID]
        n3 = self.NODES[n3ID]
        n4 = self.NODES[n4ID]
        n5 = self.NODES[n5ID]
        n6 = self.NODES[n6ID]

        # Determine shell normal vector
        v1 = np.zeros(3)
        v2 = np.zeros(3)

        v1[0] = n3['x']-n1['x']
        v1[1] = n3['y']-n1['y']
        v1[2] = n3['z']-n1['z']

        v2[0] = n4['x']-n2['x']
        v2[1] = n4['y']-n2['y']
        v2[2] = n4['z']-n2['z']

        v3 = np.cross(v1, v2)
        v4 = v3*(1/np.linalg.norm(v3))

        # Determine friction beam element shell through thickness offset to Gauss Point Locations
        gp_x =  0.577350269
        delta_in = (-gp_x)*v4*(self.wall_shell_thickness/2)
        delta_out = (gp_x)*v4*(self.wall_shell_thickness/2)

        # West Element creation
        if self.west_beam_write:
            # Create new Nodes for friction beam elements
            self.NID_max += 1       # N1 West Inside beam
            n7ID = self.NID_max
            n7Coord = nodeShift(n5, delta_in)
            self.buildNewNode(n7ID, n7Coord[0], n7Coord[1], n7Coord[2])
    
            self.NID_max += 1       # N2 West Inside Beam
            n8ID = self.NID_max
            n8Coord = nodeShift(n4, delta_in)
            self.buildNewNode(n8ID, n8Coord[0], n8Coord[1], n8Coord[2])
    
            self.NID_max += 1       # N1 West Outside Beam
            n9ID = self.NID_max
            n9Coord = nodeShift(n5, delta_out)
            self.buildNewNode(n9ID, n9Coord[0], n9Coord[1], n9Coord[2])
    
            self.NID_max += 1       # N2 West Outside Beam
            n10ID = self.NID_max
            n10Coord = nodeShift(n4, delta_out)
            self.buildNewNode(n10ID, n10Coord[0], n10Coord[1], n10Coord[2])
    
            westTopNodes = [n4ID, n8ID, n10ID]
            westBotNodes = [n5ID, n7ID, n9ID]
    
            # Create Friction Beam Elements
            self.bid_max += 1       # West Inside Beam
            b1ID = self.bid_max
            self.buildBeam(b1ID, fricPID, str(n7ID), str(n8ID), '0')
    
            self.bid_max += 1       # West Outside Beam
            b2ID = self.bid_max
            self.buildBeam(b2ID, fricPID, str(n9ID), str(n10ID), '0')
    
            # Create NRBs connecting inside and outside friction beams
            self.set_max += 1       # West Top Connection
            self.nrb_max += 1
            s1ID = self.set_max
            nrb1ID = self.nrb_max
            self.buildSetNode(s1ID, westTopNodes)
            self.buildNRB(nrb1ID, s1ID)
    
            self.set_max += 1       # West Bottom Connection
            self.nrb_max += 1
            s2ID = self.set_max
            nrb2ID = self.nrb_max
            self.buildSetNode(s2ID, westBotNodes)
            self.buildNRB(nrb2ID, s2ID)

        # East Element Creation
        # Create new Nodes for friction beam elements
        self.NID_max += 1       # N1 East Inside beam
        n11ID = self.NID_max
        n11Coord = nodeShift(n6, delta_in)
        self.buildNewNode(n11ID, n11Coord[0], n11Coord[1], n11Coord[2])

        self.NID_max += 1       # N2 East Inside Beam
        n12ID = self.NID_max
        n12Coord = nodeShift(n3, delta_in)
        self.buildNewNode(n12ID, n12Coord[0], n12Coord[1], n12Coord[2])

        self.NID_max += 1       # N1 East Outside Beam
        n13ID = self.NID_max
        n13Coord = nodeShift(n6, delta_out)
        self.buildNewNode(n13ID, n13Coord[0], n13Coord[1], n13Coord[2])

        self.NID_max += 1       # N2 East Outside Beam
        n14ID = self.NID_max
        n14Coord = nodeShift(n3, delta_out)
        self.buildNewNode(n14ID, n14Coord[0], n14Coord[1], n14Coord[2])

        eastTopNodes = [n3ID, n12ID, n14ID]
        eastBotNodes = [n6ID, n11ID, n13ID]

        # Create Friction Beam Elements
        self.bid_max += 1       # East Inside Beam
        b3ID = self.bid_max
        self.buildBeam(b3ID, fricPID, str(n11ID), str(n12ID), '0')

        self.bid_max += 1       # East Outside Beam
        b4ID = self.bid_max
        self.buildBeam(b4ID, fricPID, str(n13ID), str(n14ID), '0')

        # Create NRBs connecting inside and outside friction beams
        self.set_max += 1       # East Top Connection
        self.nrb_max += 1
        s3ID = self.set_max
        nrb3ID = self.nrb_max
        self.buildSetNode(s3ID, eastTopNodes)
        self.buildNRB(nrb3ID, s3ID)

        self.set_max += 1       # East Bottom Connection
        self.nrb_max += 1
        s4ID = self.set_max
        nrb4ID = self.nrb_max
        self.buildSetNode(s4ID, eastBotNodes)
        self.buildNRB(nrb4ID, s4ID)

    def buildCohesiveBedJoints(self):
        """
        This function creates the new cohesive elements to represent the bed 
        joint interface and updates the node typology for existing shell elements.

        For two shell elements and a new cohesive element:

                n4              n3
                x_______________x
                |               |
                |               |
                |   Top Shell   |
                |               |
             n1 x_______________x n2
                |  Cohesive El. |
             m4 x_______________x m3
                |               |
                |               |
                |  Bot. Shell   |
                |               |
             m1 x_______________x m2

        The topology ordering for the cohesive element is m4, m3, n2, n1
        """

        if self.skipped_rows > 0:
            currCourses = self.brickCoursesReduced
        else:
            currCourses = self.brickCourses

        bottomCourse = min(currCourses)
        #topCourse = max(currCourses)
        for k in currCourses:
            sortedThetas = list(self.theta_buckets[k].keys())
            sortedThetas.sort()

            if self.debugging_bed:
                perimeter = [[],[]]

            # Loop over each brick course going from the bottom to the top (cylindrical z coordinate)
            if k == bottomCourse:
                # Loop around the brick course based upon cylindircal theta coordinate
                noTheta = 1
                for bed in sortedThetas:
                    sid = self.theta_buckets[k][bed]

                    n1ID = self.SHELLS[sid]['n1']
                    n2ID = self.SHELLS[sid]['n2']

                    # Create new bottom nodes for cohesive element
                    if noTheta == 1:
                        self.buildCoincidentNode(n1ID)
                        m4ID = str(self.NID_max)

                        self.buildCoincidentNode(n2ID)
                        m3ID = str(self.NID_max)
                        
                        noTheta += 1

                        first_cohSid = str(self.sid_max+1)
                    elif bed == sortedThetas[-1]:
                        # Need to identify previous cohesive shell m4ID and m3ID
                        prevCohSID = str(self.sid_max)#-1)
                        m4ID = self.SHELLS[prevCohSID]['n2']
                        m3ID = self.SHELLS[first_cohSid]['n1']

                    else:
                        # Need to identify previous cohesive shell m4ID
                        prevCohSID = str(self.sid_max)#-1)
                        m4ID = self.SHELLS[prevCohSID]['n2']

                        self.buildCoincidentNode(n2ID)
                        m3ID = str(self.NID_max)

                    self.buildCohesive(self.bedJointPID, m4ID, m3ID, n2ID, n1ID)
                    self.SHELLS[sid]['BED_ref'] = str(self.sid_max)

                    if self.debugging_bed and bed == sortedThetas[-1]:
                        import matplotlib.pyplot as plt

                        # startID = 2994; end ID = 976
                        shell1 = self.SHELLS[self.theta_buckets[k][bed]]
                        cohes1 = self.SHELLS[str(self.sid_max)]
                        n1 = self.NODES[shell1['n1']]
                        n2 = self.NODES[shell1['n2']]
                        n3 = self.NODES[shell1['n3']]
                        n4 = self.NODES[shell1['n4']]
                        n5 = self.NODES[cohes1['n1']]
                        n6 = self.NODES[cohes1['n2']]

                        perimeter[0].append(shell1['x'])
                        perimeter[1].append(shell1['y'])

                        
                        plt.figure(num=1)
                        plt.title("ELEMENT LAYOUT")
                        plt.xlabel("x-coord [in]")
                        plt.ylabel("z-coord [in]")
                        plt.scatter(n1['y'], n1['z'], color=[1,0,0])
                        plt.scatter(n2['y'], n2['z'], color=[1,0,0])
                        plt.scatter(n3['y'], n3['z'], color=[1,0,0])
                        plt.scatter(n4['y'], n4['z'], color=[1,0,0])
                        plt.scatter(n5['y'], n5['z']-0.5, color=[0,1,0])
                        plt.scatter(n6['y'], n6['z']-0.5, color=[0,0,1])
                        
                        """
                        plt.figure(num=2)
                        plt.title("Theta Check")
                        plt.xlabel("X-Coord")
                        plt.ylabel("Y-Coord")
                        plt.scatter(perimeter[0], perimeter[1], color=[1,0,0])
                        plt.xlim((-10, 180))
                        plt.ylim((-10, 250))
                        """

                        plt.show()

            #elif k == topCourse:
            #    return
            else:
                # Loop around the brick course based upon cylindircal theta coordinate
                noTheta = 1
                for bed in sortedThetas:
                    sid = self.theta_buckets[k][bed]
                    
                    n1ID = self.SHELLS[sid]['n1']
                    n2ID = self.SHELLS[sid]['n2']
                    edgeSID = self.sharedEdge(n1ID, n2ID, sid, 'XREF')
                    edgeShell = self.SHELLS[edgeSID]

                    # Create new bottom nodes for cohesive element and shared edge bottom element
                    if noTheta == 1:
                        self.buildCoincidentNode(n1ID)
                        m4ID = str(self.NID_max)

                        self.buildCoincidentNode(n2ID)
                        m3ID = str(self.NID_max)

                        noTheta += 1

                        first_cohSid = str(self.sid_max+1)
                    elif bed == sortedThetas[-1]:
                        # Need to identify previous cohesive shell m4ID and m3ID
                        prevCohSID = str(self.sid_max)
                        m4ID = self.SHELLS[prevCohSID]['n2']
                        m3ID = self.SHELLS[first_cohSid]['n1']

                    else:
                        # Need to identify previous cohesive shell m4ID
                        prevCohSID = str(self.sid_max)
                        m4ID = self.SHELLS[prevCohSID]['n2']

                        self.buildCoincidentNode(n2ID)
                        m3ID = str(self.NID_max)
                    
                    edgeShell['m4'] = m4ID
                    edgeShell['m3'] = m3ID

                    self.buildCohesive(self.bedJointPID, m4ID, m3ID, n2ID, n1ID)
                    self.SHELLS[sid]['BED_ref'] = str(self.sid_max)

                    if self.debugging_bed and bed == sortedThetas[-1]:
                        import matplotlib.pyplot as plt

                        # startID = 2994; end ID = 976
                        shell1 = self.SHELLS[self.theta_buckets[k][bed]]
                        cohes1 = self.SHELLS[str(self.sid_max)]
                        n1 = self.NODES[shell1['n1']]
                        n2 = self.NODES[shell1['n2']]
                        n3 = self.NODES[shell1['n3']]
                        n4 = self.NODES[shell1['n4']]
                        n5 = self.NODES[cohes1['n1']]
                        n6 = self.NODES[cohes1['n2']]

                        perimeter[0].append(shell1['x'])
                        perimeter[1].append(shell1['y'])

                        
                        plt.figure(num=1)
                        plt.title("ELEMENT LAYOUT")
                        plt.xlabel("x-coord [in]")
                        plt.ylabel("z-coord [in]")
                        plt.scatter(n1['y'], n1['z'], color=[1,0,0])
                        plt.scatter(n2['y'], n2['z'], color=[1,0,0])
                        plt.scatter(n3['y'], n3['z'], color=[1,0,0])
                        plt.scatter(n4['y'], n4['z'], color=[1,0,0])
                        plt.scatter(n5['y'], n5['z']-0.5, color=[0,1,0])
                        plt.scatter(n6['y'], n6['z']-0.5, color=[0,0,1])
                        
                        """
                        plt.figure(num=2)
                        plt.title("Theta Check")
                        plt.xlabel("X-Coord")
                        plt.ylabel("Y-Coord")
                        plt.scatter(perimeter[0], perimeter[1], color=[1,0,0])
                        plt.xlim((-10, 180))
                        plt.ylim((-10, 250))
                        """

                        plt.show()

    def buildCohesiveHeadJoints(self):
        """
        This function creates the new cohesive elements to represent the head
        joint interfaces and updates the node typology for existing shell elements.

        For two shell elements and a new cohesive element:

                x-------------x-----x-------------x
                |             |     |             |
                |          m8 |     | m7          |          Labels this row if rows are skipped
              n4|          n3 |     | m4          |m3
                x-------------x-----x-------------x
                |             |  h  |             |
                |             |  e  |             |
                |   Shell 1   |  a  |   Shell 2   |
                |   Shell 3   |  d  |   Shell 4   |          Labels this row if rows are skipped
                |             |  e  |             |
                |             |  r  |             |
                x-------------x-----x-------------x
             n1 |          n2 |     | m1          | m2
                |          m5 |     | m6          |          Labels this row if rows are skipped
                |             |     |             |
                x-------------x-----x-------------x

        The topology ordering for the cohesive element is m1, m4, n3, n2
        If rows are skipped, cohesive tology ordering will be m6, m7, m8, m5
        """
        
        self.updateXREF()
        topCourse = max(self.brickCourses)
        for k in range(0, len(self.brickCourses)):
            if (k != 0) and (round(k % (self.skipped_rows+1)) != 0):
                continue
            
            course = self.brickCourses[k]
            if self.skipped_rows > 0:
                skippedCourses = []
                for e in range(1, (self.skipped_rows+1)):
                    try:
                        skippedCourses.append(self.brickCourses[k+e])
                    except:
                        continue
            
            if self.skipped_rows > 0:
                currThetas = self.theta_buckets_reduced[course]
                sortedThetasAll = list(self.theta_buckets[course].keys())
                sortedThetasAll.sort()
                
                nextThetas = []
                nextThetasAll = []
                sortedThetasNext = []
                sortedThetasNextAll = []
                for e in range(0, len(skippedCourses)):
                    nextThetas.append(self.theta_buckets_reduced[skippedCourses[e]])
                    nextThetasAll.append(self.theta_buckets[skippedCourses[e]])
                    
                    sortedThetasCurrent = list(self.theta_buckets[skippedCourses[e]].keys())
                    sortedThetasCurrent.sort()
                    sortedThetasNextAll.append(sortedThetasCurrent)
                    
                    sortedThetasCurrent = list(self.theta_buckets_reduced[skippedCourses[e]].keys())
                    sortedThetasCurrent.sort()
                    sortedThetasNext.append(sortedThetasCurrent)
            else:
                currThetas = self.theta_buckets[course]
            
            sortedThetas = list(currThetas.keys())
            sortedThetas.sort()

            if self.debugging_head:
                perimeter = [[],[]]

            for j in range(0, len(sortedThetas)):
                """
                Loop around perimeter (in cylindrical coordinate, theta) to modify each 
                brick in current course. May need to offset joint location in theta to 
                prevent unintentionally forming an expansion joint of vertically aligned 
                mortar joints.

                sf = some scale factor (3?)
                for j in range((0-sf*k), (len(sortedThetas)-sf*k))

                Identify whether current edge will be a head joint or brick splitting joint
                These joints alternate in diagonal patterns (| = head; || = brick split)

                     ____  _____ ____  _____
                    |____||_____|____||_____|
                   ||_____|____||_____|____||
                    |____||_____|____||_____|

                This will be determined by summing the remainder of k/2 + the remainder of j/2
                If the result is 0 (even) the joint will be 
                """
                
                if self.skipped_rows > 0:
                    courseInd = floor(k/(self.skipped_rows+1))%2
                else:
                    courseInd = k%2
                    
                if (courseInd+j)%2 > 1e-6:
                    headPID = self.headJointPID
                else:
                    headPID = self.brickSplitPID

                head1 = sortedThetas[j]
                if self.skipped_rows > 0:
                    head2Ind = round((j*(self.skipped_rows+1))-1)
                    head2 = sortedThetasAll[head2Ind]
                        
                else:
                    head2 = sortedThetas[j-1]
                
                sid1 = self.theta_buckets[course][head1]
                sid2 = self.theta_buckets[course][head2]
                
                if sid1 == sid2:
                    continue

                m1ID = self.SHELLS[sid1]['n1']
                try:
                    m4ID = self.SHELLS[sid1]['m4']
                except:
                    m4ID = self.SHELLS[sid1]['n4']

                self.buildCoincidentNode(m1ID)
                n2ID = str(self.NID_max)

                self.buildCoincidentNode(m4ID)
                n3ID = str(self.NID_max)

                self.buildCohesive(headPID, m1ID, m4ID, n3ID, n2ID)
                self.SHELLS[sid2]['HEAD_ref'] = str(self.sid_max)

                # Determine nodes IDs on left shell (-theta coord)
                try:
                    n1_id = self.SHELLS[sid2]['m1']
                except:
                    n1_id = self.SHELLS[sid2]['n1']
                try:
                    n2_id = self.SHELLS[sid2]['m2']
                except:
                    n2_id = self.SHELLS[sid2]['n2']
                try:
                    n3_id = self.SHELLS[sid2]['m3']
                except:
                    n3_id = self.SHELLS[sid2]['n3']
                try:
                    n4_id = self.SHELLS[sid2]['m4']
                except:
                    n4_id = self.SHELLS[sid2]['n4']
                
                botLeftID = self.sharedEdge(n1_id, n2_id, sid2, 'HEAD_XREF')

                if course != topCourse:
                    topLeftID = self.sharedEdge(n4_id, n3_id, sid2, 'HEAD_XREF')

                    # Update top cohesion bed joint topology
                    self.SHELLS[topLeftID]['m2'] = n3ID
                
                # Update bottom cohesion bed joint topology
                self.SHELLS[botLeftID]['m3'] = n2ID

                # Update shell joint topology
                self.SHELLS[sid2]['m2'] = n2ID
                self.SHELLS[sid2]['m3'] = n3ID
                
                # Build header joints in skipped courses
                if self.skipped_rows > 0:
                    head3 = []
                    head4 = []
                    for e in range(0, len(skippedCourses)):
                        head3.append(sortedThetasNext[e][j])
                        head4.append(sortedThetasNextAll[e][head2Ind])
                        
                    sid3 = []
                    sid4 = []
                    for e in range(0, len(skippedCourses)):
                        sid3.append(self.theta_buckets[skippedCourses[e]][head3[e]])
                        sid4.append(self.theta_buckets[skippedCourses[e]][head4[e]])
                    
                    try:
                        m5ID = self.SHELLS[sid2]['m3']
                    except:
                        m5ID = self.SHELLS[sid2]['n3']
                    try:
                        m6ID = self.SHELLS[sid1]['m4']
                    except:
                        m6ID = self.SHELLS[sid1]['n4']
                        
                    m5IDs = [m5ID]
                    m6IDs = [m6ID]
                    m7IDs = []
                    m8IDs = []
                    for e in range(0, len(skippedCourses)):
                        try:
                            m7ID = self.SHELLS[sid3[e]]['m4']
                        except:
                            m7ID = self.SHELLS[sid3[e]]['n4']
                        
                        self.buildCoincidentNode(m7ID)
                        m8ID = str(self.NID_max)
                        
                        m7IDs.append(m7ID)
                        m8IDs.append(m8ID)
                        
                        self.buildCohesive(headPID, m6IDs[e], m7IDs[e], m8IDs[e], m5IDs[e])
                        self.SHELLS[sid4[e]]['HEAD_ref'] = str(self.sid_max)
                        
                        if self.skipped_rows > 1:
                            m5IDs.append(m8IDs[e])
                            m6IDs.append(m7IDs[e])
                        
                        # Update brick shell element topology
                        brickShellID = sid4[e]
                        self.SHELLS[brickShellID]['m3'] = m8IDs[e]
        
                        if skippedCourses[e] != topCourse:
                            if self.skipped_rows > 1:
                                # Update nodes of brick shell element above
                                sidUP = self.theta_buckets[skippedCourses[e]][head4[e]]
                                self.SHELLS[sidUP]['m2'] = m8IDs[e]
                            else:
                                # Update nodes of cohesion element above
                                try:
                                    nextCourse = self.brickCourses[k+(e+1)+1]
                                    upperThetas = self.theta_buckets[nextCourse]
                                    brickAbove = upperThetas[head4[e]]
                                    topLeftID = self.SHELLS[brickAbove]['BED_ref']
    
                                    # Update top cohesion bed joint topology
                                    self.SHELLS[topLeftID]['m2'] = m8IDs[e]
                                except:
                                    if skippedCourses[e] not in self.skipped_rows_print_suppression.keys():
                                        self.bs_counter+=1
                                        print("No course available above current brick course."
                                              " Some nodes not merged at top nodes of %s. "
                                              "There have been %i nodes skipped.\n"
                                              "Review Shell element %s"
                                              % (skippedCourses[e], self.bs_counter, sid4[e]))
                                        self.skipped_rows_print_suppression[skippedCourses[e]] = True
                                        continue
        
                        # Update shell joint topology
                        #self.SHELLS[sid2]['m2'] = m5IDs[e]
                        #self.SHELLS[sid2]['m3'] = m8IDs[e]
                        
                        
    
    def buildFrictionBedJoints(self) -> None:
        """
        This function loops over brick shell elements and creates friction
        elements spanning cohesive bed joint elements. Reference 
        buildCohesiveBedJoints() function description for labeling scheme.
        """

        # Loop over each brick course
        for k in self.brickCourses:
            sortedThetas = list(self.theta_buckets[k].keys())
            sortedThetas.sort()
            
            # Loop around brick course in cylindrical coordinates for each brick
            for bed in sortedThetas:
                sid = self.theta_buckets[k][bed]
                bedID = self.SHELLS[sid]['BED_ref']

                # If no bed joint element defined, continue to next brick element
                if bedID == '':
                    continue

                try:
                    n4ID = self.SHELLS[sid]['m4']
                except:
                    n4ID = self.SHELLS[sid]['n4']
                try:
                    n3ID = self.SHELLS[sid]['m3']
                except:
                    n3ID = self.SHELLS[sid]['n3']
                try:
                    n2ID = self.SHELLS[bedID]['m3']
                except:
                    n2ID = self.SHELLS[bedID]['n3']
                try:
                    n1ID = self.SHELLS[bedID]['m4']
                except:
                    n1ID = self.SHELLS[bedID]['n4']
                try:
                    m4ID = self.SHELLS[bedID]['m1']
                except:
                    m4ID = self.SHELLS[bedID]['n1']
                try:
                    m3ID = self.SHELLS[bedID]['m2']
                except:
                    m3ID = self.SHELLS[bedID]['n2']

                nodeList = [n4ID, n3ID, n2ID, n1ID, m4ID, m3ID]
                self.buildFricBeam(nodeList, self.fricBedPID)

    def buildFrictionHeadJoints(self) -> None:
        """
        This function loops over brick shell elements and creates friction
        elements spanning cohesive head joint elements and cohesive brick
        splitting elements. Reference buildCohesiveHeadJoints() function
        description for labeling scheme.
        """

        # Loop over each brick course
        for k in range(0, len(self.brickCourses)):
            course = self.brickCourses[k]
            sortedThetas = list(self.theta_buckets[course].keys())
            sortedThetas.sort()

            if self.debugging_head:
                perimeter = [[],[]]

            for j in range(0, len(sortedThetas)):
                head1 = sortedThetas[j-1]
                head2 = sortedThetas[j]
                sid1 = self.theta_buckets[course][head1]
                sid2 = self.theta_buckets[course][head2]
                headID = self.SHELLS[sid1]['HEAD_ref']

                # If no bed joint element defined, continue to next brick element
                if headID == '':
                    continue

                try:
                    n4ID = self.SHELLS[sid2]['m3']
                except:
                    n4ID = self.SHELLS[sid2]['n3']
                try:
                    n3ID = self.SHELLS[sid2]['m2']
                except:
                    n3ID = self.SHELLS[sid2]['n2']
                try:
                    n2ID = self.SHELLS[headID]['m1']
                except:
                    n2ID = self.SHELLS[headID]['n1']
                try:
                    n1ID = self.SHELLS[headID]['m2']
                except:
                    n1ID = self.SHELLS[headID]['n2']
                try:
                    m4ID = self.SHELLS[headID]['m3']
                except:
                    m4ID = self.SHELLS[headID]['n3']
                try:
                    m3ID = self.SHELLS[headID]['m4']
                except:
                    m3ID = self.SHELLS[headID]['n4']

                nodeList = [n4ID, n3ID, n2ID, n1ID, m4ID, m3ID]
                self.buildFricBeam(nodeList, self.fricHeadPID)

    def updateXREF(self) -> None:
        """
        This function populates the node dictionary aray 'HEAD_XREF' to be valid
        for the updated shell geometry (after bed joint creation).
        """

        for shID in self.SHELLS.keys():
            n1ID = self.SHELLS[shID]['n1']
            n2ID = self.SHELLS[shID]['n2']
            try:
                n3ID = self.SHELLS[shID]['m3']
            except:
                n3ID = self.SHELLS[shID]['n3']
            try:
                n4ID = self.SHELLS[shID]['m4']
            except:
                n4ID = self.SHELLS[shID]['n4']

            self.NODES[n1ID]['HEAD_XREF'].append(shID)
            self.NODES[n2ID]['HEAD_XREF'].append(shID)
            self.NODES[n3ID]['HEAD_XREF'].append(shID)
            self.NODES[n4ID]['HEAD_XREF'].append(shID)

    def sharedEdge(self, nid1: str, nid2: str, sid: str, refKey: str) -> str:
        """
        This function recieves and element ID and the NIDs comprising a particular
        edge in the element. The other element ID attached to this edge is then
        returned.
        """
            
        def tooMany(nid1, nid2, sid):
            raise ValueError("Too many edges found for Shell Element ID %s given nodes %s and %s" % (sid, nid1, nid2))
        
        def tooFew(nid1, nid2, sid):
            raise ValueError("No edge found for Shell Element ID %s given nodes %s and %s" % (sid, nid1, nid2))
            
        attached = [k for k in self.NODES[nid1][refKey] if k != sid]    # Find other shells at node, nid1
        otherShl = [k for k in self.NODES[nid2][refKey] if k != sid]    # Find other shells at node, nid2

        overlap = list(set(attached) & set(otherShl))
        dimOverlap = len(overlap)

        if dimOverlap == 0:
            tooFew(nid1, nid2, sid)
        elif dimOverlap == 1:
            edgeSID = overlap[0]
        else:
            nonCohesive = []

            for k in overlap:
                if self.SHELLS[k]['pid'] in self.cohesivePIDall:
                    continue
                else:
                    nonCohesive.append(k)
            
            dimNonCohesive = len(nonCohesive)
            if dimNonCohesive == 0:
                tooFew(nid1, nid2, sid)
            elif dimNonCohesive == 1:
                edgeSID = nonCohesive[0]
            else:
                tooMany(nid1, nid2, sid)

        return edgeSID

    def writeDynaHeader(self) -> list:
        from datetime import datetime

        header = []
        timeNow = (datetime.now().strftime("%d/%m/%Y %H:%M"))
        header.append("$# LS-DYNA Keyword file created using Python from Hypermesh OptiStruct export.\n")
        header.append("$ Created on %s\n" % timeNow)
        header.append("$ See script optiStuctParser.py\n")
        header.append("$\n")
        header.append("*KEYWORD\n")
        header.append("$\n")
        
        return header

    def writeDynaNodes(self) -> None:
        self.modelFile.append("*NODE\n")

        for nid in self.NODES.keys():
            node = self.NODES[nid]
            
            line = nid.rjust(8)

            xVal = node['x']
            yVal = node['y']
            zVal = node['z']

            xStr = str(xVal).rjust(16)
            yStr = str(yVal).rjust(16)
            zStr = str(zVal).rjust(16)

            if len(xStr) > 16:
                xStr = ('%1.9e' % xVal).rjust(16)
            if len(yStr) > 16:
                yStr = ('%1.9e' % yVal).rjust(16)
            if len(zStr) > 16:
                zStr = ('%1.9e' % zVal).rjust(16)

            line += xStr
            line += yStr
            line += zStr
            line += '       0       0\n'

            self.modelFile.append(line)

        self.modelFile.append("$\n")
        self.modelFile.append("$\n")

    def writeDynaShells(self) -> None:
        self.modelFile.append("*ELEMENT_SHELL\n")

        for sid in self.SHELLS.keys():
            shell = self.SHELLS[sid]

            line = sid.rjust(8)
            line += str(shell['pid']).rjust(8)
            try:
                line += shell['m1'].rjust(8)
            except:
                line += shell['n1'].rjust(8)
            try:
                line += shell['m2'].rjust(8)
            except:
                line += shell['n2'].rjust(8)
            try:
                line += shell['m3'].rjust(8)
            except:
                line += shell['n3'].rjust(8)
            try:
                line += shell['m4'].rjust(8)
            except:
                line += shell['n4'].rjust(8)
            line += '       0       0       0       0\n'

            self.modelFile.append(line)

        self.modelFile.append("$\n")
        self.modelFile.append("$\n")

    def writeDynaBeams(self) -> None:
        """
        This function compiles beam keywords for LS-DYNA input file writing.
        """

        self.modelFile.append("*ELEMENT_BEAM\n")

        for bid in self.BEAMS.keys():
            beam = self.BEAMS[bid]

            line = bid.rjust(8)
            line += str(beam['pid']).rjust(8)
            line += beam['n1'].rjust(8)
            line += beam['n2'].rjust(8)
            line += beam['n3'].rjust(8)
            line += '\n'

            self.modelFile.append(line)

        self.modelFile.append("$\n")
        self.modelFile.append("$\n")

    def writeDynaSetNodes(self) -> None:
        """
        This function compiles SET_NODE keywords for LS-DYNA input file writing.
        """

        for setID in self.SET_NODES.keys():
            self.modelFile.append("*SET_NODE_LIST\n")
            set_node = self.SET_NODES[setID]
            
            line1 = setID.rjust(10)
            line1 += "       0.0       0.0       0.0       0.0\n"
            self.modelFile.append(line1)

            for nodeLine in set_node['nodeList']:
                self.modelFile.append(nodeLine + '\n')

            self.modelFile.append('$\n')

        self.modelFile.append("$\n")
        self.modelFile.append("$\n")

    def writeDynaNRBs(self) -> None:
        """
        This function compiles CONSTRAINED_NODAL_RIGID_BODY keywords for LS-DYNA
        input file writing.
        """

        for nrbID in self.NRBS.keys():
            self.modelFile.append("*CONSTRAINED_NODAL_RIGID_BODY\n")
            nrb = self.NRBS[nrbID]

            line = nrbID.rjust(10)
            line += '         0'
            line += nrb['setID'].rjust(10)
            line += '         0         0         0         0\n'
            self.modelFile.append(line)
            self.modelFile.append('$\n')

        self.modelFile.append("$\n")
        self.modelFile.append("$\n")

    def writeDyna(self, fName: str) -> None:
        """
        This function writes the LS-DYNA file with cohesive elements line by line.
        """

        self.modelFile = self.writeDynaHeader()
        self.writeDynaNodes()
        self.writeDynaShells()
        self.writeDynaBeams()
        self.writeDynaSetNodes()
        self.writeDynaNRBs()
        self.modelFile.append('*END')

        with open(fName, 'w') as f:
            for l in self.modelFile:
                f.writelines(l)



if __name__ == '__main__':
    from os.path import join, dirname, realpath

    fPath = dirname(realpath(__file__))
    fName = 'Wall_Mesh.key'
    outName = 'cohesive_wall.key'

    fTarget = join(fPath, fName)
    fOutput = join(fPath, outName)

    walls = dynaModel()
    walls.skipped_rows = 1
    walls.theta_offest = 0.02

    with open(fTarget, 'r') as f:
        lines = f.readlines()
        for line in lines:
            walls.parseLine(line)

    walls.cylindSort()
    print("Building centroid = %0.1f, %0.1f, %0.1f" % (walls.modelX,walls.modelY,walls.modelZ))
    
    walls.cohesivePIDs(101, 102, 103)
    walls.wall_shell_thickness = 8.0
    walls.set_max = 10001
    walls.nrb_max = 10001
    walls.bid_max = 10001
    walls.fricBeamPIDs()
    walls.buildCohesiveBedJoints()
    walls.buildCohesiveHeadJoints()
    #walls.buildFrictionBedJoints()
    #walls.buildFrictionHeadJoints()
    walls.writeDyna(fOutput)
    

    